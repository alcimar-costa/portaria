from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as login_portaria, logout as logout_portaria

from visitante.models import Visitante


def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')

    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username=username, password=password)

    if user:
        login_portaria(request, user)
        return redirect('dashboard')

    mensagem = 'Usuário ou senha invalido!'
    return render(request, 'login.html', {'mensagem': mensagem})


@login_required(login_url='login')
def logout(request):
    logout_portaria(request)
    return redirect('login')


@login_required(login_url='login')
def dashboard(request):
    visitantes = Visitante.objects.all()
    qtd_mulheres = len(visitantes.filter(sexo__icontains="F"))
    qtd_homens = len(visitantes.filter(sexo__icontains="M"))
    context = {
        'qtd_mulheres': qtd_mulheres,
        'qtd_homens': qtd_homens,

    }

    return render(request, 'dashboard/dashboard.html', context)

