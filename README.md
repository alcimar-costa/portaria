# Sistema para controle de acesso - PORTARIA


## Como baixar e utilizar

Primeiramente, verifique se instalou de forma certa o Python 3, pip e o virtualenv


```
python -V
pip -V
virtualenv --version

``` 

Para iniciar o projeto é necessário que você crie uma máquina virtual e instale as dependências
do projeto.

Primeiramente crie um diretório em um caminho mais próximo a raiz do seu sistema operacioal, 
que facilite o acesso, sem caracteres especiais até o diretório do projeto.

```
 mkdir projetos_django
```
Agora entre no diretório recem criado e baixe o projeto do repositório e o descompacte.

Crie uma máquina virtual na raiz do diretório criado para baixar o projeto, de forma que 
não fique dentro do projeto baixado, mas lado a lado. É importante que a máquina virtual 
tenha a versão 3 do python.

Caso tenha mais de uma versão do python no seu sistema operacional, é necessário que especifique
a versão 3 do pythom para a criação da máquina virtual.

Faça isso com o comando: 
```
 which python3
```

será exibido algo como:

```
 /usr/local/bin/python3
```

Agora que já sabe a localização do python 3 no seus sistema operacional, execute o comando:

```
 virtualenv --python=/usr/local/bin/pyton3 .venv
```

Agora que você já tem o projeto baixado e sua máquina virtual criada, vamos iniciar a máquina virtual
para que possamos instalar as dependencias do projeto que se encontram no arquivo 
requirements.txt. Utilize o comando:

```
 pip install requirements.txt
```
Agora estamos com as dependências instaladas e podemos rodar os comandos:

```
 python manage.py migrate
 python manage.py runserver
```

O o primeiro comando acima, você terá criado o banco de dados sqlite3 com as tabelas e com o segundo 
comando, terá iniciado o projeto rodando no endereço http://127.0.0.1:8000/


ATENÇÃO: como o projeto foi configurado para ter acesso através de um email, você terá 
que, no momento de criar o usuário, no campo username, informar um email. Isso você fará logo abaixo.

Crie um úsuário para autenticaçao no sistema com o comando:
```
 python manage.py createsuperuser
```


Para inserir os dados básicos no banco de dados, foi criado um arquivo na raiz do projeto 
de nome data.json. 
Para que esses dados iniciais sejam salvos no banco de dados, você precisará
apenas digitar o comando no terminal:

```
python manage.py loaddata data.json 

```  

Clonar o repositório

```
git clone https://alcimar-costa@bitbucket.org/alcimar-costa/portaria.git
```  