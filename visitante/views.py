from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db.models import Q
from visitante.forms import VisitanteForm
from visitante.models import Visitante


@login_required(login_url='login')
def listar(request):

    search_item = request.GET.get('pesquisa' or None)

    if search_item:
        visitantes = Visitante.objects.filter(Q(nome__icontains=search_item) | Q(cpf__icontains=search_item) |
                                              Q(rg__icontains=search_item))
    else:
        visitantes = Visitante.objects.all().order_by('nome')
        paginator = Paginator(visitantes, 5)
        page = request.GET.get('page')
        visitantes = paginator.get_page(page)

    context = {
        'visitantes': visitantes
    }

    return render(request, 'visitante/listar.html', context)


@login_required(login_url='login')
def criar(request):
    form = VisitanteForm(request.POST or None, request.FILES or None)
    visitantes = Visitante.objects.all().order_by('-id')
    paginator = Paginator(visitantes, 5)
    page = request.GET.get('page')
    visitantes = paginator.get_page(page)

    if form.is_valid():
        form.save()
        messages.success(request, "Cadastro realizado com sucesso!")
        return redirect('visitante_listar')
    context = {
        'form': form,
        'visitantes': visitantes
    }
    return render(request, 'visitante/formulario.html', context)


@login_required(login_url='login')
def atualizar(request, id):
    visitante = get_object_or_404(Visitante, pk=id)
    form = VisitanteForm(request.POST or None, request.FILES or None, instance=visitante)
    visitantes = Visitante.objects.all()
    paginator = Paginator(visitantes, 5)
    page = request.GET.get('page')
    visitantes = paginator.get_page(page)

    if form.is_valid():
        form.save()
        messages.success(request, "Cadastro atualizado com sucesso!")
        return redirect('visitante_listar')

    context = {
        'visitantes': visitantes,
        'form': form
    }

    return render(request, 'visitante/formulario.html', context)


@login_required(login_url='login')
def deletar(request, id):
    visitante = get_object_or_404(Visitante, pk=id)
    visitantes = Visitante.objects.all()
    paginator = Paginator(visitantes, 5)
    page = request.GET.get('page')
    visitantes = paginator.get_page(page)

    if request.method == 'POST':
        visitante.delete()
        messages.success(request, "Cadastro removido com sucesso!")
        return redirect('visitante_listar')

    context = {
        'visitantes': visitantes,
        'visitante': visitante
    }
    return render(request, 'visitante/formulario_confirmar_deletar.html', context)


@login_required(login_url='login')
def detalhar(request, id):
    visitante = get_object_or_404(Visitante, pk=id)
    acessos = visitante.acesso_set.all().order_by('-id')
    paginator = Paginator(acessos, 5)
    page = request.GET.get('page')
    acessos = paginator.get_page(page)
    variaveis ={
        'visitante':visitante,
        'acessos': acessos
    }

    return render(request, 'visitante/detalhe.html', variaveis)
