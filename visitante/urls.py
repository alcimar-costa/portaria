from django.urls import path
from .views import listar, criar, atualizar, deletar, detalhar

urlpatterns = [
    path('listar/', listar, name='visitante_listar'),
    path('criar/', criar, name='visitante_criar'),
    path('atualizar/<int:id>/', atualizar , name='visitante_atualizar'),
    path('deletar/<int:id>/', deletar , name='visitante_deletar'),
    path('detalhar/<int:id>/', detalhar , name='visitante_detalhar')
]
