from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from datetime import date

from acesso.forms import AcessoFormulario, AcessoFormularioCompleto
from acesso.models import Acesso
from visitante.models import Visitante


def salvar(request, id):

    data = dict()
    data_atual = date.today()

    if request.method == 'POST':

        if form.is_valid():
            visitante = get_object_or_404(Visitante, id=visitante_id)
            formulario_temp = form.save(commit=False)
            formulario_temp.visitante = visitante
            formulario_temp.data_entrada = data_atual
            formulario_temp.save()
            acessos = visitante.acesso_set.all().order_by('-id')
            paginator = Paginator(acessos, 5)
            page = request.GET.get('page')
            acessos = paginator.get_page(page)

            data['form_is_valid'] = True

            data['acesso_mensagem'] = render_to_string('acesso/mensagem.html',
                                                              {'mensagem': mensagem})
            data['acessos'] = render_to_string('acesso/acesso_lista_fragmento.html',
                                                           {'acessos': acessos})
        else:
            data['form_is_valid'] = False

    context = {
        'form': form,
        'visitante_id': visitante_id
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def criar(request, id):

    form = AcessoFormulario(request.POST or None, request.FILES or None)
    mensagem = ''
    visitante_id = id

    if request.method == 'POST':
        mensagem = 'Acesso salvo com sucesso!'

    return salvar(request, form, 'acesso/acesso_criar.html', mensagem, visitante_id)


def atualizar(request, id):

    mensagem = ''
    acesso = get_object_or_404(Acesso, id=id)
    visitante_id = acesso.visitante.id

    if request.method == 'POST':
        form = AcessoFormulario(request.POST or None, request.FILES or None, instance=acesso)
        mensagem = 'Acesso atualizado com sucesso!'
    else:
        form = AcessoFormulario(request.POST or None, request.FILES or None, instance=acesso)
    return salvar(request, form, 'acesso/acesso_atualizar.html', mensagem, visitante_id)


def deletar(request, id):

    data = dict()
    acesso = get_object_or_404(Acesso, id=id)

    if request.method == "POST":
        acesso.delete()
        data['form_is_valid'] = True
        visitante_id = acesso.visitante.id
        visitante = get_object_or_404(Visitante, id=visitante_id)
        acessos = visitante.acesso_set.all().order_by('-id')
        paginator = Paginator(acessos, 5)
        page = request.GET.get('page')
        acessos = paginator.get_page(page)
        mensagem = 'Acesso excluido com sucesso!'
        data['acessos'] = render_to_string('acesso/acesso_lista_fragmento.html', {'acessos': acessos})
        data['acesso_mensagem'] = render_to_string('acesso/mensagem.html', {'mensagem': mensagem})
    else:
        context = {'acesso': acesso}
        data['html_form'] = render_to_string('acesso/acesso_deletar.html', context, request=request)

    return JsonResponse(data)


def sair(request, id):

    data = dict()
    acesso = get_object_or_404(Acesso, id=id)
    visitante_id = acesso.visitante.id
    template_name = 'acesso/acesso_sair.html'

    form = AcessoFormulario(request.POST or None, instance=acesso)

    if request.method == 'POST':
        if form.is_valid():
            data['form_is_valid'] = True
            visitante = get_object_or_404(Visitante, id=visitante_id)
            formulario_temp = form.save(commit=False)
            formulario_temp.visitante = visitante
            data_atual = date.today()
            formulario_temp.data_saida = data_atual
            formulario_temp.save()
            acessos = visitante.acesso_set.all().order_by('-id')
            paginator = Paginator(acessos, 5)
            page = request.GET.get('page')
            acessos = paginator.get_page(page)
            mensagem = 'Saída registrada'

            data['acesso_mensagem'] = render_to_string('acesso/mensagem.html',
                                               {'mensagem': mensagem})
            data['acessos'] = render_to_string('acesso/acesso_lista_fragmento.html',
                                           {'acessos': acessos})

    context = {
        'form': form,
        'visitante_id': visitante_id
    }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def sair_sem_modal(request, id):
    data = dict()
    acesso = get_object_or_404(Acesso, id=id)
    form = AcessoFormularioCompleto(instance=acesso)
    formulario_temp = form.save(commit=False)
    data_atual = date.today()
    formulario_temp.data_saida = data_atual
    formulario_temp.save()
    mensagem = 'Saída registrada'
    visitante_id = acesso.visitante.id
    visitante = get_object_or_404(Visitante, id=visitante_id)
    acessos = visitante.acesso_set.all().order_by('-id')
    data['acesso_mensagem'] = render_to_string('acesso/mensagem.html',
                                               {'mensagem': mensagem})
    data['acessos'] = render_to_string('acesso/acesso_lista_fragmento.html',
                                       {'acessos': acessos})
    data['form_is_valid'] = True
    return JsonResponse(data)




