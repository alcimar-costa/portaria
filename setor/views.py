from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.db.models import Q

from setor.forms import SetorForm
from setor.models import Setor


@login_required(login_url='login')
def list(request):
    search_item = request.GET.get('pesquisa', None)

    if search_item:
        setores = Setor.objects.filter(Q(nome__icontains=search_item) | Q(sigla__icontains=search_item) |
                                              Q(telefone__icontains=search_item))
    else:
        setores = Setor.objects.all().order_by('-id')

    paginator = Paginator(setores, 5)
    page = request.GET.get('page')
    setores = paginator.get_page(page)

    context = {
        'setores': setores
    }

    return render(request, 'setor/listar.html', context)

@login_required(login_url='login')
def create(request):
    form = SetorForm(request.POST or None)
    setores = Setor.objects.all().order_by('-id')

    if form.is_valid():
        form.save()
        messages.success(request, "Cadastro realizado com sucesso!")
        return redirect('setor_listar')
    context = {
        'form': form,
        'setores': setores
    }
    return render(request, 'setor/form.html', context)


@login_required(login_url='login')
def update(request, id):
    setor = get_object_or_404(Setor, pk=id)
    form = SetorForm(request.POST or None, instance=setor)
    setores = Setor.objects.all().order_by('-id')

    if form.is_valid():
        form.save()
        messages.success(request, "Cadastro atualizado com sucesso!")
        return redirect('setor_listar')

    context = {
        'setores': setores,
        'form': form
    }

    return render(request, 'setor/form.html', context)


@login_required(login_url='login')
def delete(request, id):
    setor = get_object_or_404(Setor, pk=id)
    setores = Setor.objects.all().order_by('-id')

    if request.method == 'POST':
        setores = Setor.objects.all().order_by('-id')
        setor.delete()
        messages.success(request, "Cadastro removido com sucesso!")
        return redirect('setor_listar')

    context = {
        'setores': setores,
        'setor': setor
    }
    return render(request, 'setor/form_confirm_delete.html', context)
